# UniversalSockets
Unity MonoBehaviours for thread-safe socket communication which will build on Windows, Android and UWP. The communication is handled on separate threads but incoming messages are dispached on the Unity main thread.

## Compatibility
Tested on Unity 2017.3.1

## Examples
For usage examples look at the `Tests`

## Hints
* exclude WSAPlayer for `websocket-sharp.dll` in `Import Settings`
* for UWP don't forget to set the `InternetClient` capability in `PlayerSettings` -> `Publishing Settings`
