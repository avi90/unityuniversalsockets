﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if WINDOWS_UWP
using Windows.Networking.Sockets;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using System.Collections.Concurrent;
#else
using System.Net;
using System.Net.Sockets;
#endif

/// <summary>
/// A UDP socket wrapper which works for PC standalone, Android and UWP.
/// Messages are dispached within the Unity thread.
/// </summary>
public class UniversalUDPSocket : MonoBehaviour
{
    private int clientPort = 11001;
    private int serverPort = 11000;
    private string serverAddress;

    private bool connected = false;

    private Queue<MessageEventArgs> messageBuffer = new Queue<MessageEventArgs>();

    /// <summary>
    /// Dispached when a message arrives.
    /// </summary>
    public event EventHandler<MessageEventArgs> OnMessage = delegate { };

#if !WINDOWS_UWP
    private UdpClient client;
    private System.Threading.Thread readerThread;

    /// <summary>
    /// Connect and bind the socket to the given address and port.
    /// </summary>
    /// <param name="clientPort">The port under which this client will be available.</param>
    /// <param name="serverAddress">The address of the server to connect to.</param>
    /// <param name="serverPort">The port of the server to connect to.</param>
    public void Connect(int clientPort, string serverAddress, int serverPort)
    {
        this.clientPort = clientPort;
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;

        client = new UdpClient(clientPort);
        client.Connect(serverAddress, serverPort);
        connected = true;

        readerThread = new System.Threading.Thread(Receiver);
        readerThread.Start();
    }

    /// <summary>
    /// Send a message through the socket.
    /// </summary>
    /// <param name="message">The message to send.</param>
    public void Send(string message)
    {
        if (connected) // not really thread-safe
        {
            var sendBytes = System.Text.Encoding.ASCII.GetBytes(message);
            client.Send(sendBytes, sendBytes.Length);
        }
    }

    /// <summary>
    /// Disconnect from the socket.
    /// </summary>
    public void Disconnect()
    {
        Debug.Log("disconnect");
        if (client != null)
        {
            client.Close();
        }
        connected = false;
    }

    private void Receiver()
    {
        //Debug.Log("start receiver");
        IPEndPoint remoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
        while (connected)
        {
            var receiveBytes = client.Receive(ref remoteIpEndPoint); // this is a blocking call

            string message = System.Text.Encoding.ASCII.GetString(receiveBytes);
            var args = new MessageEventArgs()
            {
                data = message
            };
            lock (messageBuffer)
            {
                messageBuffer.Enqueue(args);
            }
        }
    }
#else
    private DatagramSocket client;
    private DataWriter messageWriter;
    private BlockingCollection<string> messageSendBuffer;
    private Task messageWriterTask;

    /// <summary>
    /// Connect and bind the socket to the given address and port.
    /// </summary>
    /// <param name="clientPort">The port under which this client will be available.</param>
    /// <param name="serverAddress">The address of the server to connect to.</param>
    /// <param name="serverPort">The port of the server to connect to.</param>
    public void Connect(int clientPort, string serverAddress, int serverPort)
    {
        this.clientPort = clientPort;
        this.serverAddress = serverAddress;
        this.serverPort = serverPort;

        client = new DatagramSocket();
        Task.Run(ConnectAsync);
    }

    private async Task ConnectAsync()
    {
        client.MessageReceived += (sender, e) =>
        {
            var length = e.GetDataReader().UnconsumedBufferLength;
            var message = e.GetDataReader().ReadString(length);
            var args = new MessageEventArgs()
            {
                data = message
            };

            lock (messageBuffer)
            {
                messageBuffer.Enqueue(args);
            }
        };

        await client.BindServiceNameAsync(clientPort.ToString());
        await client.ConnectAsync(new Windows.Networking.HostName(serverAddress), serverPort.ToString());

        connected = true;
        Debug.Log(client.Information.LocalAddress + " " + client.Information.LocalPort);
        Debug.Log(client.Information.RemoteAddress + " " + client.Information.RemotePort);

        messageWriter = new DataWriter(client.OutputStream);
        messageSendBuffer = new BlockingCollection<string>();
        messageWriterTask = Task.Run(MessageWriter);

        // we have to send something to start receiving *shrug*
        Send("huhu");
    }

    private async Task MessageWriter()
    {
        try
        {
            while (true)
            {
                var message = messageSendBuffer.Take();
                messageWriter.WriteString(message);
                await messageWriter.StoreAsync();
            }
        }
        catch (InvalidOperationException)
        {
            // messageBuffer complete
        }
    }

    /// <summary>
    /// Send a message through the socket.
    /// </summary>
    /// <param name="message">The message to send.</param>
    public void Send(string message)
    {
        if (messageSendBuffer != null && connected)
        {
            messageSendBuffer.Add(message);
        }
    }

    /// <summary>
    /// Disconnect from the socket.
    /// </summary>
    public void Disconnect()
    {
        Debug.Log("disconnect");

        if (messageSendBuffer != null)
        {
            messageSendBuffer.CompleteAdding();
        }

        // TODO: wait for messageSendBuffer to drain
        client.Dispose();
        connected = false;
    }
#endif

    private void OnDisable()
    {
        Disconnect();
    }

    private void Update()
    {
        // dispatch messages in Unity thread
        if (connected)
        {
            lock (messageBuffer)
            {
                while (messageBuffer.Count > 0)
                {
                    var message = messageBuffer.Dequeue();
                    OnMessage(this, message);
                    //Debug.Log(message);
                }
            }
        }
    }

    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// The content of the message.
        /// </summary>
        public string data;
    }
}
