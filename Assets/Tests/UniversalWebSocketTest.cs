﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalWebSocketTest : MonoBehaviour {
    private UniversalWebSocket webSocket;

	void Start () {
        webSocket = GetComponent<UniversalWebSocket>();

        webSocket.OnMessage += WebSocket_OnMessage;
        webSocket.Connect("ws://127.0.0.1:11000");
	}

    private void WebSocket_OnMessage(object sender, UniversalWebSocket.MessageEventArgs e)
    {
        Debug.Log(e.data);
        webSocket.Send("return " + e.data);
    }
}
