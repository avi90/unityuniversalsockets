﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UniversalUDPSocketTest : MonoBehaviour {
    private UniversalUDPSocket udpSocket;

	void Start () {
        udpSocket = GetComponent<UniversalUDPSocket>();

        udpSocket.OnMessage += Socket_OnMessage;
        udpSocket.Connect(11000, "127.0.0.1", 11001);
	}

    private void Socket_OnMessage(object sender, UniversalUDPSocket.MessageEventArgs e)
    {
        Debug.Log(e.data);
        udpSocket.Send("return " + e.data);
    }
}
