﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if !WINDOWS_UWP
using WebSocketSharp;
#else
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using System.Threading.Tasks;
using System.Collections.Concurrent;
#endif

/// <summary>
/// A websocket wrapper which works for PC standalone, Android and UWP.
/// Messages are dispached within the Unity thread.
/// </summary>
public class UniversalWebSocket : MonoBehaviour
{
    /// <summary>
    /// Dispached when a message arrives.
    /// </summary>
    public event EventHandler<MessageEventArgs> OnMessage = delegate { };
    /// <summary>
    /// Dispached when an error occurs.
    /// </summary>
    public event EventHandler<ErrorEventArgs> OnError = delegate { };
    /// <summary>
    /// Dispached when to socket is ready to communicate.
    /// </summary>
    public event EventHandler OnOpen = delegate { };

    private string address;
    private Queue<MessageEventArgs> messageQueue = new Queue<MessageEventArgs>();
    private object messageQueueLock = new object();
    private Queue<ErrorEventArgs> errorQueue = new Queue<ErrorEventArgs>();
    private object errorQueueLock = new object();
    private Queue<EventArgs> openQueue = new Queue<EventArgs>();
    private object openQueueLock = new object();

    public bool IsConnected { private set; get; }

    public UniversalWebSocket()
    {
        IsConnected = false;
    }

#if !WINDOWS_UWP
    private WebSocket webSocket;

    /// <summary>
    /// Connect to the given address.
    /// </summary>
    /// <param name="address">The address in the form of "ws://example.com".</param>
    public void Connect(string address)
    {
        // TODO: hande multiple connects correctly
        this.address = address;
        webSocket = new WebSocket(address);
        webSocket.OnMessage += (sender, e) =>
        {
            var args = new MessageEventArgs()
            {
                data = e.Data
            };
            lock (messageQueueLock)
            {
                messageQueue.Enqueue(args);
            }
        };

        webSocket.OnError += (sender, e) =>
        {
            var args = new ErrorEventArgs()
            {
                errorMessage = e.Message
            };
            lock (errorQueueLock)
            {
                errorQueue.Enqueue(args);
            }
        };

        webSocket.OnOpen += (sender, e) =>
        {
            IsConnected = true;
            lock (openQueueLock)
            {
                openQueue.Enqueue(new EventArgs());
            }
        };

        webSocket.Connect();
    }

    /// <summary>
    /// Send a message to the WebSocket.
    /// </summary>
    /// <param name="message">The message to send.</param>
    public void Send(string message)
    {
        //Debug.Log("send " + message + " " + IsConnected);
        if (IsConnected)
        {
            webSocket.Send(message);
        }
    }

    /// <summary>
    /// Disconnect from the WebSocket.
    /// </summary>
    public void Disconnect()
    {
        webSocket.Close();
        IsConnected = false;
    }


#else

    private MessageWebSocket webSocket;
    private DataWriter messageWriter;
    private BlockingCollection<string> messageSendBuffer;
    private Task senderTask;

    /// <summary>
    /// Connect to the given address.
    /// </summary>
    /// <param name="address">The address in the form "ws://example.com".</param>
    public void Connect(string address)
    {
        this.address = address;
        webSocket = new MessageWebSocket();
        webSocket.Control.MessageType = SocketMessageType.Utf8;
        Task.Factory.StartNew(ConnectAsync);
    }

    private async Task ConnectAsync()
    {
        webSocket.MessageReceived += (sender, e) =>
        {
            var messageReader = e.GetDataReader();
            messageReader.UnicodeEncoding = UnicodeEncoding.Utf8;
            string message = messageReader.ReadString(messageReader.UnconsumedBufferLength);

            var args = new MessageEventArgs
            {
                data = message
            };
            lock(messageQueueLock)
            {
                messageQueue.Enqueue(args);
            }
        };

        await webSocket.ConnectAsync(new Uri(address));
        IsConnected = true;

        messageWriter = new DataWriter(webSocket.OutputStream);
        messageSendBuffer = new BlockingCollection<string>();
        senderTask = Task.Factory.StartNew(Sender);

        lock(openQueueLock)
        {
            openQueue.Enqueue(new EventArgs());
        }
    }

    /// <summary>
    /// Send a message to the WebSocket.
    /// </summary>
    /// <param name="message">The message to send.</param>
    public void Send(string message)
    {
        if (messageSendBuffer != null && IsConnected)
        {
            messageSendBuffer.Add(message);
        }
    }

    private async Task Sender()
    {
        try
        {
            while (true)
            {
                var message = messageSendBuffer.Take();
                messageWriter.WriteString(message);
                await messageWriter.StoreAsync();
            }
        }
        catch (InvalidOperationException)
        {
            // messageBuffer complete
        }
    }

    /// <summary>
    /// Disconnect from the WebSocket.
    /// </summary>
    public void Disconnect()
    {
        if (messageSendBuffer != null)
        {
            messageSendBuffer.CompleteAdding();
        }
        // TODO: wait for messageSendBuffer to drain
        webSocket.Close(1000, "bye");
        IsConnected = false;
    }
#endif

    private void OnDisable()
    {
        Disconnect();
    }

    private void Update()
    {
        if (!IsConnected)
        {
            return;
        }

        // dispatch messages in Unity thread
        lock (messageQueueLock)
        {
            for (var i = 0; i < messageQueue.Count; i++)
            {
                var message = messageQueue.Dequeue();
                OnMessage(this, message);
            }
        }

        lock (errorQueueLock)
        {
            for (var i = 0; i < errorQueue.Count; i++)
            {
                var message = errorQueue.Dequeue();
                OnError(this, message);
            }
        }

        lock (openQueueLock)
        {
            for (var i = 0; i < openQueue.Count; i++)
            {
                var message = openQueue.Dequeue();
                OnOpen(this, message);
            }
        }
    }

    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// The content of the message.
        /// </summary>
        public string data;
    }

    public class ErrorEventArgs : EventArgs
    {
        /// <summary>
        /// A brief description of the error.
        /// </summary>
        public string errorMessage;
    }
}
